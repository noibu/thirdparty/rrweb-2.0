import * as rollup from 'rollup';
import * as typescript from 'rollup-plugin-typescript2';
import resolve from '@rollup/plugin-node-resolve';
const _typescript = typescript as unknown as typeof typescript.default;

/**
 * Use rollup to compile an input TS script into JS code string.
 */
export async function compileTSCode(
  inputFilePath: string,
  retriesLeft: number = 3,
): Promise<string> {
  try {
    const bundle = await rollup.rollup({
      input: inputFilePath,
      plugins: [
        resolve() as unknown as rollup.Plugin,
        _typescript({
          tsconfigOverride: { compilerOptions: { module: 'ESNext' } },
        }) as unknown as rollup.Plugin,
      ],
    });
    const {
      output: [{ code: _code }],
    } = await bundle.generate({
      name: 'rrdom',
      format: 'iife',
    });
    return _code;
  } catch (e) {
    // retrying
    if (retriesLeft > 0) {
      console.log(
        `Retrying compileTSCode('${inputFilePath}', ${retriesLeft - 1})`,
      );
      return compileTSCode(inputFilePath, retriesLeft - 1);
    }
    throw e;
  }
}
